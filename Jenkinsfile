pipeline {

    agent any
    parameters {
        // Tests to run
        //API
        string(name: 'apiTests', defaultValue: 'ApiTestsIT' , description: 'API Tests' )
        // Cucumber
        string(name: 'cuke', defaultValue: 'RunCukesIT' , description: 'Cucumber Tests' )

        // General
        string(name: 'context', defaultValue: 'safebear' , description: 'application context' )
        string(name: 'domain', defaultValue: 'http://34.210.120.72' , description: 'application context' )

        // Test environment
        string(name: 'test_hostname', defaultValue: '34.210.120.72' , description: 'hostname of the test environment' )
        string(name: 'test_port', defaultValue: '8888' , description: 'port of the test environment' )
        string(name: 'test_username', defaultValue: 'tomcat' , description: 'username of the test environment' )
        string(name: 'test_password', defaultValue: 'tomcat' , description: 'password of the test environment' )

        // Browser stuff
        string(name: 'browser', defaultValue: 'headless' , description: 'browser for our gui tests' )
    }

    options {
        buildDiscarder (logRotator(numToKeepStr:'3', artifactNumToKeepStr: '3'))
    }

    triggers { pollSCM('* * * * *') } // poll the source code repo every minute

    //Pipeline Stages1
    stages{

        stage('Build with Unit Testing'){
            /* run the mvn package command to ensure build the app and run the unit */
            steps {
                sh 'mvn clean package sonar:sonar -Dsonar.organization=safebear-student-bitbucket -Dsonar.host.url=https://sonarcloud.io -Dsonar.login=f08a36f3598527bf9b20d9b861b57c0aaa5937e3'
//                sh 'mvn clean package'
            }
            post {
            /* ony run if the last step succeeds */
            success {
                // print a message to screen that we're archiving
                echo 'Now Archiving.....'
                // archive the artifacts so we can build once and then use them later to deploy
                archiveArtifacts artifacts: '**/target/*.war'
                }

               always {
                   junit "**/target/surefire-reports/*.xml"
                    }
                  }

            }

        stage('Static Analysis') {
            /* run the mvn checkstyle:checkstyle command to run the static analysis. Can be done in parallel to the
            Build and Unit Testing step. */
            steps {
                sh 'mvn checkstyle:checkstyle'
            }
            post {
                success {
                    checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', unHealthy: ''
                }
            }
        }

        stage('Deploy to Test'){
            /* Deploy to the test environment so we can run our integration and BDD tests */
            steps{
                // deploy using the cargo plugin in the pom.xml maven file
                sh "mvn cargo:redeploy -Dcargo.hostname=${params.test_hostname} -Dcargo.servlet.port=${params.test_port} -Dcargo.username=${params.test_username} -Dcargo.password=${params.test_password}"
            }
            post {
                success {
                    echo 'Code deployed to Test Envonrment'
                }
                failure {
                    echo 'Deployment failed'
                }
            }
            }

        stage('Integration Tests'){
            steps {
                sh "mvn -Dtest=${params.apiTests} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context}"
            }
            post{
                always {
                    junit "**/target/surefire-reports/*ApiTestsIT.xml"
                }
                }
            }

        stage('BDD Requirements Testing'){

            steps{

//                sh "mvn -Dtest=${params.cuke} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context}"
                sh "mvn -Dtest=${params.cuke} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context} -Dbrowser=${params.browser}"

            }
            post {
                always{
                    publishHTML([
                            allowMissing            : false,
                            alwaysLinkToLastBuild   : false,
                            keepAll                 : false,
                            reportDir               : 'target/cucumber',
//                          reportFiles             : 'index.html',
                            reportFiles             : 'extent_report.html',
                            reportName              : 'BDD report',
                            reportTitles            : ''
                    ])
                }
            }


        }
        }

    }
