package com.safebear.tasklist.apitests;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;
import org.junit.Before;
import org.junit.Test;

public class ApiTestsIT {
    final String DOMAIN = System.getProperty("domain");
    final int PORT = Integer.parseInt(System.getProperty("port"));
    final String CONTEXT = System.getProperty("context");

    @Before
    public void setUp(){
        // setting the base URI details and the Port
        RestAssured.baseURI=DOMAIN;
        RestAssured.port=PORT;
        // register parser for the response as JSON as we're talking to an API
        RestAssured.registerParser("application/json", Parser.JSON);
    }

    @Test
    public void testHomePage(){
        //now we construct out URL for the /api/tasks endpoint
        RestAssured.get("/" + CONTEXT + "/api/tasks")
                .then()
                .assertThat()
                .statusCode(200);

    }


}

