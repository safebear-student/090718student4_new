package com.safebear.tasklist.usertests;

import com.safebear.tasklist.usertests.pages.TaskListPage;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

/**
 * Created by CCA_Student on 09/07/2018.
 */
public class StepDefs {

    // Getting the URL details
    final String DOMAIN = System.getProperty("domain");
    final String PORT = System.getProperty("port");
    final String CONTEXT = System.getProperty("context");

    WebDriver driver;
    TaskListPage taskListPage;

    @Before
    public void setUp(){
        String BROWSER = System.getProperty("browser");
        switch (BROWSER) {
            case "headless":
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--headless");
                driver = new ChromeDriver(options);
                break;
            case "chrome":
                driver = new ChromeDriver();
                break;
            default:
                driver = new ChromeDriver();
                break;
        }

        //        driver = new ChromeDriver();
        taskListPage  = new TaskListPage(driver);
        String url = DOMAIN + ":" + PORT + "/" + CONTEXT;
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        driver.quit();

        try {
            Thread.sleep(2000);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
    }



    @Given("^the following tasks are created$")
    public void the_following_tasks_are_created(DataTable arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        throw new PendingException();
    }

    @When("^the homepage opens$")
    public void the_homepage_opens() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the following tasks appear in the list:$")
    public void the_following_tasks_appear_in_the_list(DataTable arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        throw new PendingException();
    }

    @Given("^a (.+) is in a (.+)$")
    public void a_task_is_in_a_state(String taskname, String status) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^a (.+) is updated to a (.+)$")
    public void a_task_is_updated_to_a_completed(String taskname, String endstate) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the (.+) is now in the (.+)$")
    public void the_task_is_now_completed(String taskname, String endstate) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^a user creates a (.+)$")
    public void a_user_creates_a_task(String taskname) {
        taskListPage.addTask(taskname);
    }

    @Then("^the (.+) appears in the list$")
    public void the_task_appears_in_the_list(String taskname) {
        Assertions.assertThat(taskListPage.checkForTask(taskname)).isTrue();

    }

}
